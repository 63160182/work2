/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class Noodle extends Food{
    private String name;
    private double price;
    private int amount;

    public Noodle(String name, double price, int amount) {
        super(name, price, amount);
        this.name = name;
        this.price = price;
        this.amount = amount;
    }

    @Override
    public void order(){
        System.out.println("You ordered a Noodle");
        System.out.println(name+" added");
        sum=+price;
    }
  
}
